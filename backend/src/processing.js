const localIntents = {
  hello: {
    default: 'Hi'
  },
  perks_info: {
    default: 'Well with use we have offers which nobody has'
  },
  room_help: {
    default: 'Offcourse Here is the Help in the link ...'
  },
  room_options: {
    default: 'We have multitude of rooms, how would you like it, small/deluxe/Etype?'
  },
  room_booked: {
    default: 'Your room has been booked, Happy Staying at Adlon?'
  },
  ok: {
    default: 'okay...'
  },
  yes: {
    default: 'Yes Sure...'
  },
  name: {
    default: 'I am SmartBot...'
  },
  datetime: {
    default: 'Today Date...'
  }
};

const localContexts = {
  hello: {
    default: 'Hi'
  },
  myname: {
    default: 'SmartBot'
  }
};

const firstEntityValue = (entities, entity) => {
  const entityFound =
    entities &&
    entities[entity] &&
    Array.isArray(entities[entity]) &&
    entities[entity].length > 0 &&
    entities[entity][0];

  const val =
    (entityFound && entityFound.value) ||
    (entityFound && Array.isArray(entityFound.values) && entityFound.values[0].value);
  const conf = entityFound && entityFound.confidence;
  if (!val) {
    return null;
  }
  return { value: typeof val === 'object' ? val.value : val, conf };
};

const ProcessOutCome = outcome => {
  // make a request
  let reply = 'I dont know what you are saying...! ';
  let isIntentThere = Object.keys(outcome.entities).length > 0;
  let entities = outcome.entities;
  let confidence = 0.5;
  if (isIntentThere) {
    Object.keys(localIntents).some(function(o) {
      let entityFound = firstEntityValue(entities, o);
      if (entityFound) {
        if (entityFound.conf > confidence) {
          reply = o === 'datetime' ? new Date(entityFound.value).toDateString() : entityFound.value;
        } else {
          reply = localIntents[o] || initialMessage;
        }
        if (o === 'hello') {
          reply = 'Alice...' + reply;
        }

        reply += ' ... Match Level : [ ' + (entityFound.conf * 100).toFixed(0) + ' % ] ';
      }
      // return !!entityFound && confidence >= 0.5;
    });
  }
  return reply;
};
module.exports = ProcessOutCome;
