'use strict';
const MY_WIT_TOKEN = 'KVODMW2YLSFYTPQTKBKMH45MNAZTCIBB';

var app = require('express')();
var http = require('http').Server(app);
var io = require('socket.io')(http);
var SocketHandler = require('./socketHandler.js');
var fetch = require('isomorphic-fetch');
var Training = require('./training');

console.log('Training Started...');
Training();

const { Wit, log, interactive } = require('node-wit');

const client = new Wit({
  accessToken: MY_WIT_TOKEN,
  logger: new log.Logger(log.DEBUG) // optional
});

const socketHandler = new SocketHandler.SocketHandler();

io.on('connection', function(socket) {
  socketHandler.onConnect({ socket });

  socket.on('disconnect', function() {
    socketHandler.onDisconnect();
  });

  socket.on('chat message', function(msg) {
    client.message(msg).then(
      function(nlp) {
        console.log('Receievd....', nlp);
        socketHandler.onMessage({ msg, socket, nlp });
      },
      function(error) {
        socket.emit('chat message', 'Something Went wrong, Please try again');
      }
    );
  });
});

http.listen(3000, '0.0.0.0', function() {
  // eslint-disable-line no-magic-numbers
  console.log('listening on *:3000');
});
