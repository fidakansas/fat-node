const tasks = [
  {
    entity: 'perks_info',
    text: 'Why should i stay, enligten me?'
  },
  {
    entity: 'room_help',
    text: 'can you Help me with room booking?'
  },
  {
    entity: 'room_options',
    text: 'What options do you have?'
  },
  {
    entity: 'room_booked',
    text: 'Your room has been booked, Happy Staying at Adlon?'
  }
];

const entities = [
  {
    id: 'perks_info',
    values: [
      {
        value: 'We have Free Complimentary Breakfast , Offer on Weekends ,50% off for tourists ',
        expressions: ['Why should i stay, enligten me?']
      }
    ]
  },
  {
    id: 'room_help',
    values: [
      {
        value: 'Yes, Please on the Home page select the Rooms tabs and click Book',
        expressions: ['can you Help me with room booking?']
      }
    ]
  },
  {
    id: 'room_options',
    values: [
      {
        value: 'Yes, We have Deluxe, Small, Large What would you like?',
        expressions: ['What options do you have?']
      }
    ]
  },
  {
    id: 'room_booked',
    values: [
      {
        value: 'Your Large Room has been booked, Happy Staying at Adlon',
        expressions: ['Large Room']
      },
      {
        value: 'Your Small Room has been booked, Happy Staying at Adlon',
        expressions: ['Small Room']
      },
      {
        value: 'Your Deluxe Room has been booked, Happy Staying at Adlon',
        expressions: ['Deluxe Room']
      }
    ]
  }
];

const registerEntityUrl = '//api.wit.ai/entities?v=20160526';
const trainingEntityUrl = '//api.wit.ai/samples?v=20170307';
const MY_WIT_TOKEN = 'Bearer KVODMW2YLSFYTPQTKBKMH45MNAZTCIBB';
const Training = outcome => {
  var myHeaders = new Headers();
  myHeaders.append('Authorization', MY_WIT_TOKEN);
  myHeaders.append('Content-Type', 'application/json');

  // Register Relevant Entities
  entities.some(function(entity) {
    fetch(registerEntityUrl, {
      method: 'POST',
      headers: myHeaders,
      body: JSON.stringify(entity)
    })
      .then(function(response) {
        return response.json();
      })
      .then(function(json) {
        console.log('Registered ...' + entity.id);
        console.log(json);
      });
  });

  //Train your app.
  fetch(trainingEntityUrl, {
    method: 'POST',
    headers: myHeaders,
    body: JSON.stringify(tasks)
  })
    .then(function(response) {
      return response.json();
    })
    .then(function(json) {
      console.log('Training Complete...');
      console.log(json);
    });
};
module.exports = Training;
