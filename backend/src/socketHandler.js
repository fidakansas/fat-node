var ProcessOutCome = require('./processing');

/**
 * The SocketHandler class handles sockets
 */
class SocketHandler {
  /**
   * Creates a new SocketHandler
   */
  constructor() {}

  /**
   * Logs that a user connected
   */
  onConnect(/* istanbul ignore next */ { c = console, socket } = {}) {
    c.log('a user connected');
    socket.emit('chat message', 'Hi, How is may I help you?');
  }

  /**
   * Logs that a user disconnected
   */
  onDisconnect(/* istanbul ignore next */ { c = console } = {}) {
    c.log('a user disconnected');
  }

  /**
   * Responds to a message
   */
  onMessage(/* istanbul ignore next */ { msg, socket, nlp, c = console } = {}) {
    c.log('message: ' + msg);
    let reply = ProcessOutCome(nlp);
    socket.emit('chat message', reply);
  }
}

exports.SocketHandler = SocketHandler;
